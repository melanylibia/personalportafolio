+++
author = "L1bSp"
title = "diferent questions"
date = "2019-03-19"
description = "Are you ready?"
categories = [
    "Blog",
]
tags = [
    "Blog",
]
image = ""
+++

Hi friends, today i have new things to share. I'm practing for a frontend interview this is my small journy.

## **1. Form** 

I'd like to explain my code for the first project, which is about forms. 

In the Forms project, I created a component with an interface called `ContactFormData`. This interface has three properties: `name`, `email`, and `message`. 

To save the state of the form data, I used the `useState` hook with the type `ContactFormData`. 

When the user changes the input values, I created a function called `handleInputChange` to update the `formData` values. 

For submitting the data, I created another function called `handleSubmit`. This function sends the data to a REST API. 

Now, let's look at the body of our component. I created a form that includes the `handleSubmit` function. This form has three input fields: `name`, `email`, and `message`. 

Finally, there is a submit button that triggers the `handleSubmit` function when clicked.

**link the code: [formData.tsx](https://gist.github.com/38ff8d3e321099d1590ba881dcdc6565.git)**


## **2. HolyGrail**

I completed the Holy Grail Challenge, which involves designing a standard canvas. In the body of the canvas, I created a table with three rows. The first row is the header, the second row has three columns: the left sidebar, the main sidebar, and the right sidebar. The last row is the footer. To make these HTML components fit together, I needed to use CSS.

For the header, the only the padding style is important. However, for the right sidebar, main container, and left sidebar, there are some differences in their widths. Specifically, the widths are 25%, 50%, and 25%, respectively. Finally, for the footer, only the padding style was changed.

**link the code: [holygrail](https://gist.github.com/d38877874182e9a9c00d8154606fcf6f.git)**

## **3. mortage  calculator**

I created a mortgage calculator. First, I designed an interface called `MortgageCalculatorState` with six properties: `loanAmount`, `interestRate`, `loanTerm`, `monthlyPayment`, `totalPayment`, and `totalInterest`.

Next, I created a hook to store the state of my form data, which is of type `MortgageCalculatorState`. This hook helps me save the user's input.

I also wrote a function called `handleInputChange` to update the values of my hook. However, it only updates some properties that are visible to the user.

It's important to note that I used ES6 syntax to set the data, specifically the arrow function `(x)=>({...x,y,z})`.

The `calculateMortgage` function performs the logic to calculate the `totalPayment`, `totalInterest`, and `monthlyPayment`. After that, I set the new values of these three properties.

One thing to mention is that this project does not have any CSS styling.

**link the code: [MortgageCalculatorProps](https://gist.github.com/b56195afafc67cd335332fc6b26c40fd.git)**

## **4. tabs component**

I worked on a project to create a tab component. First, I defined an interface for a tab, which has three properties: an ID, a label, and content. Using this interface, I created a constant array of tabs.

Next, I created a hook called 'activeTab' to store the state of the currently active tab's index. 

In the component's body, I looped over the tabs array, rendering each tab individually. I needed to add a conditional statement to check if the activeTab matches the ID of the current tab. If it does, I applied a different style to highlight the active tab. 

I also created an anchor tag (href) for each tab. When the user clicks on a tab, it triggers the 'handleTabChange' function, which updates the value of the activeTab. This function takes one parameter, which is the ID of the tab that was clicked.

Additionally, I have a div component that renders the data of the currently active tab.

I almost forget the css style, tabs has 3 important properties display, flex-direction and align-items. A tabs-nav has 2 important properties list-style none and display flex. A last one is the class active it's just added border-bottom style for a tag remove the text-decoration to none.


**link the code: [Tabs](https://gist.github.com/8b19c85c9461f18b6606b828057dfc29.git)**


#### Any plataform has the potential to make a significant impact on the industry.

{{< css.inline >}}
<style>
.canon { background: white; width: 100%; height: auto; }
</style>
{{< /css.inline >}}
