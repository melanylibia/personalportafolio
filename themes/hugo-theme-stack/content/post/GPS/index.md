+++
author = "L1bSp"
title = "Track Me If You Can!🙈"
date = "2024-10-15"
description = "start tracking easily🙊"
categories = [
    "Blog",
]
tags = [
    "Blog",
]
image = ""
+++


## Introduction

Hello friends, I was to share with you my search for how to track myself and have my own data. There are many options about software. Try to use if they fit to your case.

## 1. Setup Tracker

I use owntracks on my android device. I set up a remote server running a webhook to listen for connection. This is code [tracking.js](https://gist.github.com/1b9d0bb54b88166adb0c0e9bcb20df37.git)

- **Preferences**: Go to the menu > preferences > connection.
- **Security**: I added more security configurations, but I won’t show here to save time. 

![](setup.jpg)


## 2. Database Setup

I figured out how to save the data and understand the importance of each column. I have SQL Queries for creating  the database [locations](https://gist.github.com/484f7f559e0262109f5f89a849da7ce8.git). If everything goes well with the conection, the server will show you this 

![](SCHEMA.png)


## 3. Additional Features

**Watson, what motivates you to seek an extension?**

I created an extension in Postgres to analyze my data further. I found some issues with the extension, which my Postgres Docker didn’t recognize. I applied the extension to the saved data to generate more analysis, which was interesting.

![](extension.png)

## Visualitaion

Integrating GeoServer with PostGIS or your extension allows for geospatial data management and visualization capabilities. That platform for serving and manipulating geospatial information over the web. This integration is essential for building applications that require dynamic mapping and spatial data analysis. If you make your own app it suit you, but in mycase it was too much.

## What I learn 

 - Postgres extensions (Postgis)🙂
 - GeoServer

## Off topic

Take care of your dog; it’s important to ensure proper care regardless of whether it is male or female. **Dog Neutering**

{{< css.inline >}}
<style>
.canon { background: white; width: 100%; height: auto; }
</style>
{{< /css.inline >}}
