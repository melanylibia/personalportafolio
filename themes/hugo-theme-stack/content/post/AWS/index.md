+++
author = "L1bSp"
title = "Awesome AWS Project Work List"
date = "2022-09-25"
description = "My Personal Learning Path"
categories = [
    "AWS"
]
tags = [
    "AWS"
]
image = ""
+++

**Description**

I made this post is to summarize all my simple projects that I did to share my learning with others, they all have a link where you can meet them.

### Github Repositories

- [AWS_S3_SDK](https://github.com/libialany/AWS_S3_-_Nodejs_SDK)

- [FileUploaderAWS](https://github.com/libialany/FileUploaderAWS)

- [aws-notas](https://github.com/libialany/aws-notas)

### Post Blog

- [Triggering AWS Lambda from Amazon SQS](https://medium.com/@elibialany/triggering-aws-lambda-from-amazon-sqs-dbe93e02c245)

- [Creating a Multi-Region Network with VPC Peering Using SGs, IGW, and RTs](https://medium.com/@elibialany/creating-a-multi-region-network-with-vpc-peering-using-sgs-igw-and-rts-c914f626b94c)

- [Set Up a WordPress Site Using EC2 and RDS](https://medium.com/@elibialany/set-up-a-wordpress-site-using-ec2-and-rds-374fe4703c85)

- [S3 Enabling Versioning](https://medium.com/@elibialany/s3-enabling-versioning-14a3240c3bc6)

- [IAM](https://medium.com/@elibialany/iam-9a09a8471d87)

- [Using EC2 Roles and Instance Profiles in AWS](https://medium.com/@elibialany/using-ec2-roles-and-instance-profiles-in-aws-38afa708c01e)

- [AWS VPC Flow Logs for Network Monitoring](https://medium.com/@elibialany/aws-vpc-flow-logs-for-network-monitoring-9b48fced376a)

- [AWS Tags and Resource Groups](https://medium.com/@elibialany/aws-tags-and-resource-groups-8c7e32f11c09)

- [AWS Disaster Recovery](https://medium.com/@elibialany/aws-disaster-recovery-bb0dcaadbc62)

### Offtopic

Lastly, if you have a dog, please remember to take good care of them.🐶

{{< css.inline >}}
<style>
.canon { background: white; width: 100%; height: auto; }
</style>
{{< /css.inline >}}
