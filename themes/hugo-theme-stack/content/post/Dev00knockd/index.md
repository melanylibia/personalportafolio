+++
author = "L1bSp"
title = "knock knock"
date = "2024-10-14"
description = "Setup a secure server"
categories = [
    "DevOps",
]
tags = [
    "DevOps",
]
image = ""
+++

Hello friend, did you know how to access your server securely using knockd to manage your iptables firewall?

## My adventure setup knockd  

**Pretty strange** 😗

To open the firewall for your SSH port, you first need  to knock on a few other ports in the correct order. By default, you knock on ports 7000, 8000, and 9000 to let the knockd service open the SSH port. It migth seem a bit unusual, but I made a video to show you how to test this.

{{< youtube "5z9U5YFY9S0" >}}

## What I learn 

### 1. Iptables

Understand the difference between `-I` and `-A` is crucial, the primary reason my setup wasn't working was due to this distinction. I had to change it from:
`command = /sbin/iptables -A INPUT -s %IP% -p tcp --dport 22 -j ACCEPT` 
to: 
`command = /sbin/iptables -I INPUT -s %IP% -p tcp --dport 22 -j ACCEPT`

**`-A INPUT` (Append)**:
   - This command appends a new rule to the end of the `INPUT` chain.
   - If there are existing rules that deny or drop packets, this new rule will only take effect if it’s reached after those rules.
   - Essentially, it means that if there are earlier rules that block the traffic, this rule might not take effect.

**`-I INPUT` (Insert)**:
   - This command inserts a new rule at the top of the `INPUT` chain.
   - The inserted rule takes precedence over all existing rules in the chain.
   - This is useful if you want to ensure that this rule is checked first, allowing traffic on port 22 before any other potentially conflicting rules.

![iptables](iptables.jpg)

### 2. Tunnels

I had issues with my tunnel, My raspberry has a lot of configuration, which I needed to configure along with some flags on my tunnel:

`tailscale up --login-server https://cc.net --accept-dns=f --accept-routes --netfilter-mode=off`

--netfilter-mode=off: This option disables Tailscale's integration with the system's netfilter (iptables) functionality. When netfilter mode is off, Tailscale won’t automatically modify firewall rules or interact with the local network stack. This might be useful if you want to manage firewall rules manually or avoid conflicts with existing configurations.

![many tunnels](tunnels.png)

### 3. Check 

Be patient it was not simple in my case, the tutorials were a good resource but it wasn't a completed solution.

![logging better](logging.jpg)

### Off Topic 

Take care of your dog; it’s important to ensure proper care regardless of whether it is male or female. **Dog Neutering**

{{< css.inline >}}
<style>
.canon { background: white; width: 100%; height: auto; }
</style>
{{< /css.inline >}}
