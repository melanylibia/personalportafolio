+++
author = "L1bSp"
title = "Two-factor authentication on your Raspberry Pi🫢"
date = "2024-10-15"
description = "Make the server accessible from the Internet"
categories = [
    "TOTP"
]
tags = [
    "TOTP"
]
image = ""
+++

Hello, friends! Today, I’d like to share what I learned about TOTP. Why i was reading a security blog [cybersafesolutions](https://cybersafesolutions.com.au/) and then I got more concius about the security of my raspberry.

## **Setup 2F to my raspberry**

I have already enabled SSH , then we need to tell the SSH daemon to enable “challenge-response” passwords. Go ahead and open the SSH config file:

`sudo nano /etc/ssh/sshd_config`

Enable challenge response by changing ChallengeResponseAuthentication from the default no to yes.

`sudo systemctl restart ssh`


Install autnticator module in your setup, in my case i installe the Google Authenticator PAM module.

`sudo apt install libpam-google-authenticator`

And then configure: 

`google-authenticator`

Be careful, Don’t move forward quite yet! Before you do anything else you should copy the emergency codes and put them somewhere safe.

Let check the video i created:

{{< youtube "" >}}

# What I Learned

1. Not only read is enougth tried to apply.
3. SSH new configuration.


## Off topic ⭐

Please take care of them.
![MaleDogNeutaring](offtopic.jpg)

{{< css.inline >}}
<style>
.canon { background: white; width: 100%; height: auto; }
</style>
{{< /css.inline >}}
