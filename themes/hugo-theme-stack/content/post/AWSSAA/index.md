+++
author = "L1bSp"
title = "I passed AWS SAA C03"
date = "2025-01-28"
description = "Here is what you need to know"
categories = [
    "AWS", "AWS SAA"
]
tags = [
    "AWS", "AWS SAA"
]
image = ""
+++

I passed AWS SAA after studying about 1 month. In this article, I will share my experience how I studied and took preparation and also some tips & tricks.

This routine is proven in a person who worked 8 hours and took care of a recently operated dog(Visiting the vet frequently).

##  Exam Registration

I got a 50% discount for taking the previous Cloud Practicioner exam. I recommend that you go for the practitioner certifications first.

## Exam day

Go ahead. Otherwise, mark and leave it to do it later

During the exam, I finished with 40 minutes left over to review the exam. Try to read the last sentences. In my case I didn't want to read the whole question so as not to tire my brain quickly

##  Preparation

- __You must practice doing labs__ I did them on my own by reading a medium post.

    👉 [practice the SAA labs](https://medium.com/@filipespacheco/aws-cloud-quest-solution-architect-role-79517cf9ea0c)

- __I practiced a lot of reading__ the AWS documentation, I only cared about having the vocabulary I was going to need and not making mistakes in the questions just because I didn't understand a word. 

- __Prepare additional study material,__ in my case I love ANKI cards I found some that were created anki cards for example [] they were exemplary to create my own and study them in my meal time from work.

    👉 [Anki SAA](https://ankiweb.net/shared/info/1394455139)

- __Using other people's study material__, using google dorks I found thousands of flashcards that helped me study in short moments. I remember grabbing the questions from the set of 20 free AWS questions and looking them up and finding lots of flashcards from others with similar questions. 

    👉 [Question of EBS](https://quizlet.com/es/510280239/kubas-aws-website-questions-ii-flash-cards/)

    👉 [Random set test](https://quizlet.com/469056714/aws-practice-test-1-12g-flash-cards/)

    👉 [Network falscards](https://quizlet.com/288446203/aws-vpc-and-network-flash-cards/)

- __Practicing with others__ or also called having a study team in my case I practiced with friends, acquaintances, members of communities helped me to teach as well as learn, it reminded me faster what I studied with others. Thank you to everyone who studied with me at random times (5:00 AM) thank you.

##  Know the documentation

Try to verify all the material you find since some mention in the official documentation that they are not used for x reason, for example of nat instances. 

## Conclusion

Having this certification I was able to gain experience in infrastructure analysis.

## My LinkedIn

Let’s connect on LinkedIn: https://www.linkedin.com/in/melany-e


