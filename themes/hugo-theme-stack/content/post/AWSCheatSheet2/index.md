+++
author = "L1bSp"
title = "AWS FAQ I forget - part 2✍️"
date = "2025-01-22"
description = "VPC & Storage are esily forgetable for my"
categories = [
    "AWS", "AWS SAA", "VPC", "Storage"
]
tags = [
    "AWS", "AWS SAA", "VPC", "Storage"
]
image = ""
+++

## VPC

### VPC Endpoint 

__use case__ Given a bucket with a vpc endpoint you create an new one, also an ec2 instance how to route the request to the new bucket.

__steps__

1. VPC ep constains a policy. That  policy  is restricted to certains3 bucket.

2. IAM role/user does not have access to new bucket s3.

__code__

[]()

__helpful links__

[configuration](https://docs.aws.amazon.com/vpc/latest/privatelink/vpc-endpoints-access.html#vpc-endpoint-policies)

### NAT gateway basic

You cannot route traffic to a NAT gateway through a VPC peering connection, a Site-to-Site VPN connection, or AWS Direct Connect. A NAT gateway cannot be used by resources on the other side of these connections.

__code__

[i could not try it beacuse my budget](#)


### FLOW LOGS

__use case__ you have VPC a subnet, in a subnet is failing, create a flow log for that specific VPC. Captures IP traffic from network interfaces in your VPC. It stores in Amazon Cloudwatch.

__helpfull links__

[vpc flow log basic](https://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/flow-logs.html#flow-logs-basics)

## Storage

## File Gateway

Keep the storage on AWS, managing Objects/files must only be accessed via the aapplication. a person must access using s3 API.

__importance__


There are two options for Volume Gateway: Cached Volumes – you store volume data in AWS, with a small portion of recently accessed data in the cache on-premises. Stored Volumes – you store the entire set of volume data on-premises and store periodic point-in-time backups (snapshots) in AWS.

__code__

[No code is for on-premises infrastructure](#)

