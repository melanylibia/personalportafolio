+++
author = "L1bSp"
title = "I believe in Yesterday"
date = "2019-03-19"
description = "Memories hanging over me"
categories = [
    "Blog",
]
tags = [
    "Blog",
]
image = ""
+++

Hi friends, today i have new things to share. My short path i had such us devops engineeer. I was inspired by yesterday songs of the beatles.

1. Oh, Now let's look the cloud

I since 2022 i was learning AWS such us as posible i can i did this repo xyz, but i will sumarize for you.

EC2 
A virtual server, you can deploy or use for whatever,  y learn about ssh also the ebs(the volume of the EC2).

IAM
creating resources you must need rigth access, i learn about users,user groups, and roles.

IAM user: An IAM user is an identity within your AWS account that has specific permissions for a single person or application.

IAM group: An IAM group is an identity that specifies a collection of IAM users.

IAM roles: An IAM role is an identity within your AWS account that has specific permissions. It's similar to an IAM user, but isn't associated with a specific person.

S3
I had a lot media material to save of my app, i use an storage object, the importance concepts i learned was CORS and the pre-signed URL.

Use a presigned URL to share or upload objects in Amazon S3 without requiring AWS security credentials or permissions.

## Amazon RDS
I took some whorshop i got hands on it, most important concepts about these on i could say backups and restores.
RDS saves the automated backups of your DB instance according to the backup retention period that you specify. If necessary, you can recover your DB instance to any point in time during the backup retention period.

## Amazon Route53
It was hard for to develop a tool which makes DNS routing i had to learn DNS record types, Health checks tools in linux, When i use route53 for me was more useful. takes me time to learn Domain concepts.

## ELB  
ELB provides load balancers which distribute incoming traffic across multiple resources inside of AWS. My firt  time was using nginx then i apply that base knowledge in AWS.

Now i know the diference between CLB vs. NLB vs. ALB.

## Amazon CloudFront
CloudFront serves static files like HTML, CSS, JS, and images through a worldwide network of edge locations. A signed URL includes additional information, for example, an expiration date and time, that gives you more control over access to your content. 

I many articles 

## Jenkins 

An awesome tool , i studied alone i learned the logic, but when i started to work i use an alternative of jenkins like Gitlab CI, i think the really importance is about to know to write a good pipeline, i write a pipeline for mozilla with those bases i get.
 
1. what is a ci

in simple terms if you try new recipe you need to test only the mass are you preparing you will not create a batch and then test every cookie.
CI is like testing your cookies frequently, added new features/code.

2. what is cd
let's imagen you will sell, you need to deliver in a box with the information, the box must effort to the client use.
CD is like preparing your cookies for delivery after testing them.

5. the stages, build , test , staging, production.

## Let's talk about grafana

A great tool to visualize time series data  an the combination in some cases with prometheus the database is a nice team, but i only used with argocd an then conect this grafana an visualize my pods, i had the task to setup the loki dashbord it's offer more metrics done, the only problem was the setup output fomart i strugled a lot.
Whats is grafana and loki ? Loki is built around the idea of only indexing metadata about your logs’ labels.

off topic:

https://www.devopsschool.com/blog/how-to-build-when-a-change-is-pushed-to-github-in-jenkins/












## **2. HolyGrail**

I completed the Holy Grail Challenge, which involves designing a standard canvas. In the body of the canvas, I created a table with three rows. The first row is the header, the second row has three columns: the left sidebar, the main sidebar, and the right sidebar. The last row is the footer. To make these HTML components fit together, I needed to use CSS.

For the header, the only the padding style is important. However, for the right sidebar, main container, and left sidebar, there are some differences in their widths. Specifically, the widths are 25%, 50%, and 25%, respectively. Finally, for the footer, only the padding style was changed.

**link the code: [holygrail](https://gist.github.com/d38877874182e9a9c00d8154606fcf6f.git)**

## **3. mortage  calculator**

I created a mortgage calculator. First, I designed an interface called `MortgageCalculatorState` with six properties: `loanAmount`, `interestRate`, `loanTerm`, `monthlyPayment`, `totalPayment`, and `totalInterest`.

Next, I created a hook to store the state of my form data, which is of type `MortgageCalculatorState`. This hook helps me save the user's input.

I also wrote a function called `handleInputChange` to update the values of my hook. However, it only updates some properties that are visible to the user.

It's important to note that I used ES6 syntax to set the data, specifically the arrow function `(x)=>({...x,y,z})`.

The `calculateMortgage` function performs the logic to calculate the `totalPayment`, `totalInterest`, and `monthlyPayment`. After that, I set the new values of these three properties.

One thing to mention is that this project does not have any CSS styling.

**link the code: [MortgageCalculatorProps](https://gist.github.com/b56195afafc67cd335332fc6b26c40fd.git)**

## **4. tabs component**

I worked on a project to create a tab component. First, I defined an interface for a tab, which has three properties: an ID, a label, and content. Using this interface, I created a constant array of tabs.

Next, I created a hook called 'activeTab' to store the state of the currently active tab's index. 

In the component's body, I looped over the tabs array, rendering each tab individually. I needed to add a conditional statement to check if the activeTab matches the ID of the current tab. If it does, I applied a different style to highlight the active tab. 

I also created an anchor tag (href) for each tab. When the user clicks on a tab, it triggers the 'handleTabChange' function, which updates the value of the activeTab. This function takes one parameter, which is the ID of the tab that was clicked.

Additionally, I have a div component that renders the data of the currently active tab.

I almost forget the css style, tabs has 3 important properties display, flex-direction and align-items. A tabs-nav has 2 important properties list-style none and display flex. A last one is the class active it's just added border-bottom style for a tag remove the text-decoration to none.


**link the code: [Tabs](https://gist.github.com/8b19c85c9461f18b6606b828057dfc29.git)**


#### Any plataform has the potential to make a significant impact on the industry.

{{< css.inline >}}
<style>
.canon { background: white; width: 100%; height: auto; }
</style>
{{< /css.inline >}}
