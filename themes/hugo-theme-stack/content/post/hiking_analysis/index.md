+++
author = "L1bSp"
title = "hiking data analysis"
date = "2021-05-01"
description = "Let's analyze some data from hiking trips!"
categories = [
    "IA",
]
tags = [
    "hiking",
    "outdoors"
]
image = ""
+++

Description:

I learn the bases then i could apply and realize my project.

Geographic data mining.

OpenStreetMap is a collaborative project most importan massive geographic database anyone can use it.
It simple to get more information and amazing post like mapsaregreat.com are done in that post there are amazing examples no code is required.

Common OSM Tags

Something you can find places there is OSM tag for places.

```
    Highways:
        highway=residential: Residential streets.
        highway=primary: Primary roads.
        highway=path: Paths, typically used for trails.

    Natural Features:
        natural=water: Bodies of water like lakes, rivers.
        natural=wood: Areas of woodland.

    Amenities:
        amenity=school: Schools.
        amenity=restaurant: Restaurants.

    Buildings:
        building=yes: General buildings.
        building=house: Residential houses.
```

Use case i want to know natural places i could only make a simple query, let look the code: 

```
import osmnx as ox

# Define the region of interest with a bounding box
north, south, east, west = 40.800, 40.700, -73.900, -74.000  # Coordinates for New York City

# Get all natural features
natural_features = ox.geometries_from_bbox(north, south, east, west, tags={'natural': True})
print("Natural Features:")
print(natural_features.head())
```
Very simple just try it!.

osmnx is a python module to interact with OSM.

### Networkx and Geoghrapical data

In today’s interconnected world, understanding networks and their structures has become essential for a myriad of applications or hiking project like this.

```
import networkx as nx
import matplotlib.pyplot as plt
G = nx.Graph()
G.add_edges_from([("A", "C"), ("B", "D"), ("C", "E")])
nx.draw(G, with_labels=True)
plt.show()
```



### references

- [Geographic data mining and visualisation for beginners - Overpass turbo tutorial](https://mapsaregreat.com/geographic-data-mining-and-visualisation-for-beginners/overpass-turbo-tutorial.html)
- [Planning the Perfect Hike with NetworkX and OpenStreetMap](https://towardsdatascience.com/planning-the-perfect-hike-with-networkx-and-openstreetmap-2fbeaded3cc6)

{{< css.inline >}}
<style>
.canon { background: white; width: 100%; height: auto; }
</style>
{{< /css.inline >}}
