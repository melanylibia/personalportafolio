+++
author = "L1bSp"
title = "k8s journy"
date = "2025-02-23"
description = "k8s adventure"
categories = [
    "k8s",
]
tags = [
    "k8s",
]
image = ""
+++

### Deploy the k8s

# create VM the template

```
qm create 8001 --name "ubuntu-2404-cloudinit-template" --ostype l26 \
    --memory 2048 \
    --agent 1 \
    --bios ovmf --machine q35 --efidisk0 local-lvm:0,pre-enrolled-keys=0 \
    --cpu host --socket 1 --cores 2 \
    --vga serial0 --serial0 socket  \
    --net0 virtio,bridge=vmbr0
```

```
qm importdisk 8001 noble-server-cloudimg-amd64.img local-lvm
qm set 8001 --scsihw virtio-scsi-pci --virtio0 local-lvm:vm-8001-disk-1,discard=on
qm set 8001 --boot order=virtio0
qm set 8001 --scsi1 local-lvm:cloudinit
```

```
cat << EOF | sudo tee /var/lib/vz/snippets/vendor.yaml
#cloud-config
runcmd:
    - apt update
    - apt install -y qemu-guest-agent
    - systemctl start qemu-guest-agent
    - reboot
# Taken from https://forum.proxmox.com/threads/combining-custom-cloud-init-with-auto-generated.59008/page-3#post-428772
EOF
```
```
qm set 8001 --cicustom "vendor=local:snippets/vendor.yaml"
qm set 8001 --tags ubuntu-template,24.04,cloudinit
qm set 8001 --ciuser test
qm set 8001 --cipassword test
qm set 8001 --sshkeys ~/.ssh/id_ed25519.pub
qm set 8001 --ipconfig0 ip=dhcp
```

```
source /etc/network/interfaces.d/*

# The loopback network interface
auto lo
iface lo inet loopback

# The primary network interface
allow-hotplug ens18
iface ens18 inet static
        address 192.168.1.15
        netmask 255.255.255.0
        gateway 192.168.1.1
        dns-nameservers 8.8.8.8 8.8.4.4

```
# setup

```
$ export KUBECONFIG=/etc/rancher/k3s/k3s.yaml
$ mkdir -p ~/.kube
$ sudo k3s kubectl config view --raw | tee ~/.kube/config
$ chmod 600 ~/.kube/config
```

# set up the agent

in the master:
```
cat /var/lib/rancher/k3s/server/node-token
K1039e227::server:123
```

```
curl -sfL https://get.k3s.io | K3S_URL=https://<master-ip>:6443 K3S_TOKEN=123 sh -
```

```
mkdir $HOME/.kube
sudo editor $HOME/.kube/config
```

# set up Metallb

```
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.13.12/config/manifests/metallb-native.yaml
```

```
apiVersion: metallb.io/v1beta1
kind: IPAddressPool
metadata:
  name: default-pool
  namespace: metallb-system
spec:
  addresses:
    - 192.168.1.200-192.168.1.250  # Replace with your desired IP range
---
apiVersion: metallb.io/v1beta1
kind: L2Advertisement
metadata:
  name: l2-advert
  namespace: metallb-system
spec:
  ipAddressPools:
    - default-pool
```


# Deploying services that use MetalLB LoadBalancer

```
nano  web-app-demo.yaml
apiVersion: v1
kind: Namespace
metadata:
  name: web
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: web-server
  namespace: web
spec:
  selector:
    matchLabels:
      app: web
  template:
    metadata:
      labels:
        app: web
    spec:
      containers:
      - name: httpd
        image: httpd:alpine
        ports:
        - containerPort: 80
---
apiVersion: v1
kind: Service
metadata:
  name: web-server-service
  namespace: web
spec:
  selector:
    app: web
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
  type: LoadBalancer
kubectl apply -f web-app-demo.yaml
```
# set static IP

```
at /etc/systemd/network/05-eth0.network 
[Match]
Name=eth0
[Network]
DHCP=no
DNS=8.8.8.8 8.8.4.4
Domains=k3s-admin
IPv6PrivacyExtensions=false

Gateway=192.168.1.1
Address=192.168.1.11/24
```

## deploy an app with mongo

In the resources there are 2 types of app.

1. Mongo Python
2. Mysql Wordpress

in the image the (1) is the lb review

![review](lb-review.png)

## DNS for Services and Pods - retrieve them by  name rather than IP.

DNS queries may be expanded using the Pod's /etc/resolv.conf. kubelet configures this file for each Pod. 

_pods records_

For example, if a Pod in the default namespace has the IP address 172.17.0.3, and the domain name for your cluster is cluster.local, then the Pod has a DNS name:

```
172-17-0-3.default.pod.cluster.local
```


## resources

[k3s](https://www.baeldung.com/ops/k3s-getting-started)

[agent problem](https://blog.programster.org/set-up-kubectl-on-your-local-machine#configuration)

[the basic tutorial](https://canthonyscott.com/setting-up-a-k3s-kubernetes-cluster-within-proxmox/)

[mongo lab](https://github.com/chrisjen83/k3s-labs/tree/master)

[wordpress lab](https://github.com/4linux/k3s-lab.git)



{{< css.inline >}}
<style>
.canon { background: white; width: 100%; height: auto; }
</style>
{{< /css.inline >}}
