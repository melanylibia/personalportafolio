+++
author = "L1bSp"
title = "Learning Bots"
date = "2024-08-02"
description = "Bot adventure"
categories = [
    "Bot",
]
tags = [
    "Bot",
]
image = ""
+++

### My history...

As a developer, I recently create  a Telegram bot in Python that can improve README.md of any repository geven a user. While the idea seemed straightforward, I soon realized that it can do more, I had some challenges, I'll share the obstacles I faced and how I overcame them.

{{< youtube "lixdQIGyXBU" >}}

### Challenge 1: Handling Versions

One of the biggest challenges I faced was the version of the module. My base code was very simple, and when I wanted to add complexity, I discovered that the features I needed were not in that version. For example, the 'topics' feature in Telegram wasn’t available in the version I used for the initial code. I eventually rewrote the code that solution worked seamlessly.

![rewrite](oprah-rewriting.jpg)

### Challenge 2: The API limit

To improve the README, I needed to use AI to analyze and enhance it. I had to learn a tool for rate limiting because I didn’t have enough tokens. To avoid this step, my budget didn’t allow for it. It was a steep learning curve, but the end result was worth it

I did a video:

{{< youtube "TktktqlX67k" >}}

### **Link of my project:** [readmehelper](https://readmehelper.mlibia.xyz)

### Offtopic

Lastly, if you have a dog, please remember to take good care of them.🐶

{{< css.inline >}}
<style>
.canon { background: white; width: 100%; height: auto; }
</style>
{{< /css.inline >}}
