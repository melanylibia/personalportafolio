+++
author = "L1bSp"
title = "My Journey Contributing to Open Source"
date = "2024-07-20"
description = "Unexpected Reward"
categories = [
    "Blog"
]
tags = [
    "Contributing"
]
image = "contributed1.png"
+++

### Unexpected Reward: My Journey Contributing to Open Source

🚀**Introduction:**

As an enthusiastic learner, I've always been fascinated by learning how the code of amazing software works. In my case, __I just want to say I fell in love with open source.__

I'll share my experience contributing to mozdownload, the lessons I learned, and the unexpected rewards that came with it.

✴️**Getting Started ...**

I chose to contribute to **🫴[mozdownload](https://github.com/mozilla/mozdownload)** because I was already using a similar software. Also, it was an amazing community that can help anyone who is starting. I read the post on that forum. Before browsing the project's on GitHub.

I created a fork of the repository, made the necessary changes to a project similar to that. I created a __simple demo__ before make a PR.

✍️**Lessons Learned ...**

- My first submit, I did a mistake. I did not ask enough. I just commented on what I wanted and then put a lot of code, assuming the workflow and the target.

- The second submit, I got the answer with a lot of changes to the repository, the version, the space, and the maintainer's guide. A lot of details were sent to me, and the maintainer was a kind person.

- The third submit, I just made a little mistake with a step. Hopefully, the maintainer did not block me, but he told me again the step might be in a different way and sent a detailed post from a blog, which was amazing.

Working, I learned to articulate my thoughts, listen to feedback, and adapt to changing requirements.

🚀**Unexpected Rewards ...**

Contributing to mozdownload brought several unexpected rewards. Firstly, I was forced to write safe code. Secondly, I gained a sense of accomplishment and pride. Finally, my passion for open source grew, and I was willing to help and learn from each other.


✴️**Advice ...**

* Start small and be patient. Contributing to open source can take some time.

**Conclusion ...**

Contributing to mozdownload has been an incredibly rewarding experience that has taught me valuable skills. If you're interested in open source, I encourage you to take the first step. Just do it.

[my contribution](https://github.com/mozilla/mozdownload/graphs/contributors) 🌱🌱🌱

![contributing](contributing.png)


{{< css.inline >}}
<style>
.canon { background: white; width: 100%; height: auto; }
</style>
{{< /css.inline >}}
