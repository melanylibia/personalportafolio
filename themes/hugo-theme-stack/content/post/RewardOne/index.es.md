+++
author = "L1bEn"
title = "Mi viaje contribuyendo al código abierto"
date = "2019-03-20"
description = "Recompensa inesperada"
categories = [
    "Blog"
]
tags = [
    "Contribucion"
]
image = "contributed1.png"
+++

### Recompensa inesperada: Mi viaje contribuyendo al código abierto

🚀**Introducción:**

Como aprendiz entusiasta, siempre me ha fascinado aprender cómo funciona el código de un software increíble. En mi caso, sólo quiero decir que me enamoré del código abierto.

Compartiré mi experiencia contribuyendo a mozdownload, las lecciones que aprendí y las recompensas inesperadas que vinieron con ello.

✴️**Empezando ...**

Decidí contribuir a **🫴[mozdownload](https://github.com/mozilla/mozdownload)** porque ya utilizaba un software similar. Además, era una comunidad increíble que puede ayudar a cualquiera que esté empezando. Leí el post en ese foro. Antes de navegar por el proyecto en GitHub,

Con una mezcla de emoción y temor, creé un fork del repositorio, hice los cambios necesarios en un proyecto similar a ese, como una caja de arena, porque tenía miedo de enviar código incorrecto.

✍️**Lecciones aprendidas ...**

En mi primer envío cometí un error; no pregunté lo suficiente. Me limité a comentar lo que quería y luego puse un montón de código, asumiendo el flujo de trabajo y el objetivo.

El segundo envío, recibí la respuesta con un montón de cambios en el repositorio, la versión, el espacio, y la guía del mantenedor. Me enviaron muchos detalles, y el mantenedor era una persona amable.

En el tercer envío, cometí un pequeño error con un paso. Afortunadamente, el mantenedor no me bloqueó, pero me dijo de nuevo que el paso podría ser de otra manera y me envió un post detallado de un blog, lo cual fue increíble.

Aprendí la importancia de la comunicación. Trabajando, aprendí a articular mis pensamientos, escuchar los comentarios y adaptarme a los requisitos cambiantes.

🚀**Recompensas inesperadas ...**

Contribuir a mozdownload me trajo varias recompensas inesperadas. En primer lugar, me vi obligado a escribir código seguro. En segundo lugar, me sentí realizado y orgulloso. Por último, creció mi pasión por el código abierto, y me sentí dispuesto a ayudar y aprender de los demás.

✴️**Consejos**

* Empieza poco a poco y ten paciencia. Contribuir al código abierto puede llevar algún tiempo.

🔚**Conclusión**

Contribuir a mozdownload ha sido una experiencia increíblemente gratificante que me ha enseñado habilidades valiosas. Si te interesa el código abierto, te animo a que des el primer paso. Hazlo.

{{< css.inline >}}
<style>
.canon { background: white; width: 100%; height: auto; }
</style>
{{< /css.inline >}}
