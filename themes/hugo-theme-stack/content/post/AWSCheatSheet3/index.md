+++
author = "L1bSp"
title = "AWS Learning- part 3✍️"
date = "2025-01-25"
description = "Storage"
categories = [
    "AWS", "AWS SAA", "Storage"
]
tags = [
    "AWS", "AWS SAA", "Storage"
]
image = ""
+++

### IOPS vs Throughput

I just did a simple resumen of the stackover flow answer.

- IOPS - velocity.
- throughput - quantity.

### SSD 

Volumes are optimized for transactional workloads involving frequent read/write operations with small I/O size. Import point IOPS.

1. General Purpose SSD 

    - Development and test environments

2. Provisioned IOPS SSD

    - I/O-intensive database workloads

__note__

- Max IOPS: Max IOPS (Maximum Input/Output Operations Per Second) for Amazon Elastic Block Store (EBS) refers to the highest number of read and write operations that an EBS volume can handle in one second
- Max throughput: Max throughput for Amazon Elastic Block Store (EBS) refers to the maximum rate at which data can be read from or written to an EBS volume, typically measured in megabytes per second (MB/s).

### HDD

HDD-backed volumes are optimized for large streaming workloads where the dominant performance attribute is throughput.

1. Throughput Optimized HDD

    - Big data

2.  Cold HD

    - Throughput-oriented storage for data that is infrequently  accessed. Lowest storage cost is important

### Amazon EBS-optimized instance types

Use an optimized configuration, dedicated bandwidth for Amazon EBS I/O. Non-compliant periods are approximately uniformly distributed, targeting 99 percent of expected total throughput each hour.


### Let's practice

**QUIZ 1- Question to practice:**

Q1,3,8,18,25. 


**Random Questions**

O.3 A legacy application needs to interact with local storage using iSCSI. A team needs to design a reliable storage solution to provision all new storage on AWS.Which storage solution meets the legacy application requirements?
- (A) AWS Snowball storage for the legacy application until the application can be re-architected.
- (B) AWS Storage Gateway in cached mode for the legacy application storage to write data to Amazon S3.
- (C) AWS Storage Gateway in stored mode for the legacy application storage to write data to Amazon S3.
- (D) An Amazon S3 volume mounted on the legacy application server locally using the File Gateway service.
*Answer :* C



NO.14 A popular e-commerce application runs on AWS. The application encounters performance issues. The database is unable to handle the amount of queries and load during peak times. The database is running on the RDS Aurora engine on the largest instance size available.What should an administrator do to improve performance?
- (A) Convert the database to Amazon Redshift.
- (B) Create a CloudFront distribution.
- (C) Convert the database to use EBS Provisioned IOPS.
- (D) Create one or more read replicas.



NO.26 Developers are creating a new online transaction processing (OLTP) application for a small database that is very read-write intensive. A single table in the database is updated continuously throughout the day, and the developers want to ensure that the database performance is consistent.Which Amazon EBS storage option will achieve the MOST consistent performance to help maintain application performance?
- (A) Provisioned IOPS SSD
- (B) General Purpose SSD
- (C) Cold HDD
- (D) Throughput Optimized HDD



NO.27 A Solutions Architect is designing a log-processing solution that requires storage that supports up to 500 MB/s throughput. The data is sequentially accessed by an Amazon EC2 instance.Which Amazon storage type satisfies these requirements?
- (A) EBS Provisioned IOPS SSD (io1)
- (B) EBS General Purpose SSD (gp2)
- (C) EBS Throughput Optimized HDD (st1)
- (D) EBS Cold HDD (sc1)
*Answer :* C



NO.51 A Solutions Architect is designing a solution for a media company that will stream large amounts of data from an Amazon EC2 instance. The data streams are typically large and sequential, and must be able to support up to 500 MB/s.Which storage type will meet the performance requirements of this application?
- (A) EBS Provisioned IOPS SSD
- (B) EBS General Purpose SSD
- (C) EBS Cold HDD
- (D) EBS Throughput Optimized HDD



NO.58 A Solutions Architect is building an application on AWS that will require 20,000 IOPS on a particular volume to support a media event. Once the event ends, theIOPS need is no longer required. The marketing team asks the Architect to build the platform to optimize storage without incurring downtime.How should the Architect design the platform to meet these requirements?
- (A) Change the Amazon EC2 instant types.
- (B) Change the EBS volume type to Provisioned IOPS.
- (C) Stop the Amazon EC2 instance and provision IOPS for the EBS volume.
- (D) Enable an API Gateway to change the endpoints for the Amazon EC2 instances.



NO.78 An AWS workload in a VPC is running a legacy database on an Amazon EC2 instance. Data is stored on a 200GB Amazon EBS (gp2) volume. At peak load times, logs show excessive wait time.What solution should be implemented to improve database performance using persistent storage?
- (A) Migrate the data on the Amazon EBS volume to an SSD-backed volume.
- (B) Change the EC2 instance type to one with EC2 instance store volumes.
- (C) Migrate the data on the EBS volume to provisioned IOPS SSD (io1).
- (D) Change the EC2 instance type to one with burstable performance.
*Answer :* D



NO.90 A Solutions Architect must select the storage type for a big data application that requires very high sequential I/O. The data must persist if the instance is stopped.Which of the following storage types will provide the best fit at the LOWEST cost for the application?
- (A) An Amazon EC2 instance store local SSD volume.
- (B) An Amazon EBS provisioned IOPS SSD volume.
- (C) An Amazon EBS throughput optimized HDD volume.
- (D) An Amazon EBS general purpose SSD volume.
*Answer :* C



NO.102 A Solutions Architect notices slower response times from an application. The CloudWatch metrics on the MySQL RDS indicate Read IOPS are high and fluctuate significantly when the database is under load.How should the database environment be re-designed to resolve the IOPS fluctuation?
- (A) Change the RDS instance type to get more RAM.
- (B) Change the storage type to Provisioned IOPS.
- (C) Scale the web server tier horizontally.
- (D) Split the DB layer into separate RDS instances.
*Answer :* B



NO.201 A Solutions Architect is trying to bring a data warehouse workload to an Amazon EC2 instance. The data will reside in Amazon EBS volumes and full table scans will be executed frequently.What type of Amazon EBS volume would be most suitable in this scenario?
- (A) Throughput Optimized HDD (st1)
- (B) Provisioned IOPS SSD (io1)
- (C) General Purpose SSD (gp2)
- (D) Cold HDD (sc1)
*Answer :* A


NO.209 An application running on Amazon EC2 has been experiencing performance issues when accessing an Amazon RDS for Oracle database. The database has been provisioned correctly for average workloads, but there are several usage spikes each day that have saturated the database, causing the application to time out. The application is write-heavy, updating information more often than reading information. A Solutions Architect has been asked to review the application design.What should the Solutions Architect recommend to improve performance?
- (A) Put an Amazon ElastiCache cluster in front of the database and use lazy loading to limit database access during peak periods.
- (B) Put an Amazon Elasticsearch domain in front of the database and use a Write-Through cache to reduce database access during peak periods.
- (C) Configure an Amazon RDS Auto Scaling group to automatically scale the RDS instance during load spikes.
- (D) Change the Amazon RDS instance storage type from General Purpose SSD to provisioned IOPS SSD.
*Answer :* D




NO.220 A company requires operating system permission on a relational database server.What should a Solutions Architect suggest as a configuration for a highly available database architecture?
- (A) Multiple EC2 instances in a database replication configuration that uses two Availability Zones.
- (B) A standalone Amazon EC2 instance with a selected database installed.
- (C) Amazon RDS in a Multi-AZ configuration with Provisioned IOPS.
- (D) Multiple EC2 instances in a replication configuration that uses two placement groups.
*Answer :* A



NO.237 A data-processing application runs on an i3.large EC2 instance with a single 100 GB EBS gp2 volume. The application stores temporary data in a small database(less than 30 GB) located on the EBS root volume. The application is struggling to process the data fast enough, and a Solutions Architect has determined that theI/O speed of the temporary database is the bottleneck.What is the MOST cost-efficient way to improve the database response times?
- (A) Enable EBS optimization on the instance and keep the temporary files on the existing volume.
- (B) Put the temporary database on a new 50-GB EBS gp2 volume.
- (C) Move the temporary database onto instance storage.
- (D) Put the temporary database on a new 50-GB EBS io1 volume with a 3-K IOPS provision.
*Answer :* D



NO.247 An application is scanning an Amazon DynamoDB table that was created with default settings. The application occasionally reads stale data when it queries the table.How can this issue be corrected?
- (A) Increase the provisioned read capacity of the table.
- (B) Enable AutoScaling on the DynamoDB table.
- (C) Update the application to use strongly consistent reads.
- (D) Re-create the DynamoDB table with eventual consistency disabled.
*Answer :* C



NO.292 A customer has a legacy application with a large amount of data. The files accessed by the application are approximately 10 GB each, but are rarely accessed.However, when files are accessed, they are retrieved sequentially. The customer is migrating the application to AWS and would like to use Amazon EC2 andAmazon EBS.What is the Least expensive EBS volume type for this use case?
- (A) Cold HDD (sc1)
- (B) Provisioned IOPS SSD (io1)
- (C) General Purpose SSD (gp2)
- (D) Throughput Optimized HDD (st1)
*Answer :* D


### Real Cases

- write throughput
[stackoverflow question](https://stackoverflow.com/questions/12832816/choosing-configuring-a-database-for-high-throughput-reliable-consistent-write)

### Resources

[Amazon EBS volume type](https://docs.aws.amazon.com/ebs/latest/userguide/ebs-volume-types.html)

[EBS-optimized instance](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-optimized.html)

[Choosing/configuring a database for high-throughput, reliable, consistent write throughput, sacrificing latency](https://stackoverflow.com/questions/59182414/iops-vs-throughput-which-one-to-use-while-choosing-aws-ebs)

[QUIZ 1](https://quizlet.com/es/510280239/kubas-aws-website-questions-ii-flash-cards/)

[QUIZ 2](https://www.4-passing-exams.com/saac01.html)


__Message__

Take care of them 🐶