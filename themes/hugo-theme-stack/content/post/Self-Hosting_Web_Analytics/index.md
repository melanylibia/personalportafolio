+++
author = "L1bSp"
title = "Alternative to Google Analytics"
date = "2024-07-22"
description = "Self-Hosting Your Own Web Analytics"
categories = [
    "OpenSource",
]
tags = ["Self-Hosting", "Open Source", "Web Analytics"]
image = ""
+++

✔️**Introduction**

I recently worked with Traefik and discovered an amazing tool at 🫴[post of 
supergeekery  blog](https://supergeekery.com/blog/plausible-analytics-on-laravel-forge-with-traefik-and-docker). I highly recommend checking it out. I decided to try it on my Raspberry Pi, and even though I encountered a bug, it didn't stop me.

✔️**Requirements**

To test this tool, you only need an internet connection. You can try it on your own network. As for storage space, I used `docker system df -v` and found that it takes around 51Mb.

✔️**Installation**

To install, clone the 🫴[plausible community edition](https://github.com/plausible/community-edition) and follow the instructions in the README file. Then, simply run `docker compose up`. If you want to enable backups, I'll explain how later.

✔️**Configuration**

If you're testing locally, use `docker ps` to find the port number. Then, go to localhost:<port> to access the tool. When you start, you'll see a form. In the main panel, you can add websites to manage visits and analytics.

💡**Funnel Analysis**

You might find this concept interesting. Funnel analysis is a way to measure how well users complete a specific action, like making a sale or registering for something. It helps you understand how users behave on your website.

{{< css.inline >}}
<style>
.canon { background: white; width: 100%; height: auto; }
</style>
{{< /css.inline >}}
