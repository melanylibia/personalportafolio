+++
author = "L1bEn"
title = "Libros y IA"
date = "2019-03-09"
description = "Descubramos algunas tecnicas"
categories = [
    "IA",
]
tags = [
    "IA",
    "libros"
]
image = ""
+++

Creo que los Google Dorks son extremadamente útiles, ya que te ayudan a encontrar información específica rápidamente. Recientemente, me topé con un repositorio increíble que llamó mi atención, que estaba etiquetado como asistente de escritura de IA. Aunque la mayoría de los repositorios aún están en sus etapas iniciales, me gustó la idea detrás de WordFlow. Pasando a otra búsqueda con palabras clave similares, encontré un resumen de PDF enfocado en marketing, que encontré un poco molesto. A continuación, busqué prompting engineer que se especializan en escritura y me encontré con un PDF divertido. Actualmente, estoy tratando de optimizar mi proceso para crear un libro sobre temas de STEAM (Ciencia, Tecnología, Ingeniería, Artes y Matemáticas).

# Recursos

[Web Scawler development](https://en.wikipedia.org/wiki/Canons_of_page_construction#Van_de_Graaf_canon)

{{< css.inline >}}
<style>
.canon { background: white; width: 100%; height: auto; }
</style>
{{< /css.inline >}}
