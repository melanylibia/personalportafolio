+++
author = "L1bSp"
title = "Books and IA"
date = "2019-03-19"
description = "Let's find out some techniques"
categories = [
    "IA",
]
tags = [
    "IA",
    "books"
]
image = ""
+++

I believe Google Dorks are extremely useful, as they help you find specific information quickly. Recently, I stumbled upon an amazing repository that caught my attention, which was tagged as an AI writing assistant. Although most repositories are still in their early stages, I liked the idea behind WordFlow, for instance. Moving on to another search with similar keywords, I found a PDF summarizer focused on marketing, which I found a bit annoying. Next, I searched for prompting engineers who specialize in writing and came across a humorous PDF. Currently, I'm trying to optimize my process for creating a book on STEAM (Science, Technology, Engineering, Arts, and Mathematics) topics.

{{< css.inline >}}
<style>
.canon { background: white; width: 100%; height: auto; }
</style>
{{< /css.inline >}}
