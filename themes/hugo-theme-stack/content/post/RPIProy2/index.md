+++
author = "L1bSp"
title = "syncplay 📹"
date = "2024-10-23"
description = "Exploring syncplay"
categories = [
    "Open Source"
]
tags = [
    "Open Source"
]
image = ""
+++

Hello friends! i like to share with you a simple project.

## **0. Introduction**

syncplay is a Client/server to synchronize media playback on mpv/VLC/MPC-HC/MPC-BE on many computers. small feature we have. 

- Practicing Docker

## **1. Lets do it**💥

As you know i really love raspberry pi, but idea to install Client/server having not ui in the raspberry. But anyways. I use my laptop and my raspberry.

Following this tutorial [How to setup Syncplay server on Ubuntu 20.04](https://nischay.pro/blog/setup-syncplay/). I realize there is a way to improve using docker.

The real repository has not a dockerfile.

Then i will do a dockerfile.

First Part:


```
ARG PYTHON="python:3.10-alpine3.18"
FROM ${PYTHON} AS builder
ENV SYNCPLAY="1.7.3"
RUN apk add gcc musl-dev libffi-dev
RUN sh -c '[ $(getconf LONG_BIT) -eq 64 ] || apk add cargo openssl-dev'
RUN wget https://github.com/Syncplay/syncplay/archive/v${SYNCPLAY}.tar.gz -O- | tar xz
WORKDIR ./syncplay-${SYNCPLAY}/
RUN cat /dev/null > requirements_gui.txt
RUN SNAPCRAFT_PART_BUILD=1 pip wheel --wheel-dir /wheels/ ./ #Build Python Wheels
```
In this part prepares a build environment for Syncplay by installing necessary dependencies, downloading the source code, and building Python wheels 

Second Part:

```
FROM ${PYTHON} AS syncplay
WORKDIR /wheels/
RUN pip wheel PyYaml #Build Wheel for PyYAML
COPY --from=builder /wheels/ /wheels/ #Copy Wheels from Builder
WORKDIR /release/local/lib/
RUN mkdir $(basename /usr/local/lib/python3.*/) && cd ./python3.*/ && \
    ls /wheels/*.whl | xargs -P0 -n1 unzip -d ./site-packages/
COPY ./boot.py /release/bin/syncplay
```

It creates necessary directories, building and installing dependencies, and copying an entry point script.

Last Part:

```
FROM ${PYTHON}
RUN sh -c '[ $(getconf LONG_BIT) -eq 64 ] || apk add --no-cache libgcc' # Conditional Package Installation
COPY --from=syncplay /release/ /usr/
ENV PYTHONUNBUFFERED=1 #not buffered output
EXPOSE 8999
WORKDIR /data/
ENTRYPOINT ["syncplay"] #Set Entry Point
```

## **2. Testing the result**🖊️

![Audio](0.jpg)

# What I Learned

1. I practice Dockerfile

## Off topic ⭐

If you are Bolivian, please take care of our forests. Let’s work together to make our city greener by planting some trees.🌱

{{< css.inline >}}
<style>
.canon { background: white; width: 100%; height: auto; }
</style>
{{< /css.inline >}}
