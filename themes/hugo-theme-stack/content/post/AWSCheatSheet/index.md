+++
author = "L1bSp"
title = "AWS FAQ I forget✍️"
date = "2025-01-22"
description = "Long memory in my brain computer"
categories = [
    "AWS", "AWS SAA"
]
tags = [
    "AWS", "AWS SAA"
]
image = ""
+++


### Amplify

Some key words: 
[amplify](https://github.com/aws-samples/amazon-appflow/tree/master/CF-template)

Managed integration service for data transfer between data sources. 
Easily exchange data wiht over +80 cloud services. By specifying a source and destination.


__for example:__
```
Source - Flow - Destination
s3               Google sheets
```

### AWS Batch

Plans, schedules, and executes your jobs compute workloads.
AWS Batch can run jobs on: - EC2, Fargate and EKS.

Parts of a batch:
Job, Job Definitions and Job Queues.

### AWS Device Farm

test aplications in multiple devices.

### Elastic transcoder 

convert videos to many formats. Also is expensive not has Cloudformation.

###  AWS catalog 

user and administrators.


###  Kinesis

Collecting, Processing and analyzing treaming data in the cloud.

There are 4 diferents types of kineasis Streams.

__Kinesis Data Streams is a real time streaming data service.__

Configure custom producers and consumers. The most  flexible data stream option.

__Firehose__

Easiest also direct integration to specific AW services.

__Data Analytics__

Run queries data througth your real time streams.

__Video Streams__

processing video in real time.

[friendly resumen info](https://blog.knoldus.com/data-streaming-with-aws-kinesis/) 

![helpfull graphic](kinesis.png)

__Learning more about SG__

[sg principles](https://docs.aws.amazon.com/vpc/latest/userguide/vpc-security-groups.html#SecurityGroupRules)


__NAT Gateways types__

Provide connectivity to the internet or external networks for AWS resources launched in a private subnet of the VPC. NAT Gateway can be of any of the following types.

- Public NAT Gateway- This can be used to provide Internet access to resources in the private subnets of the VPC. Public NAT needs to be placed in a public subnet of the VPC, and an Elastic IP address needs to be associated with this gateway.

- Private NAT Gateway- This can be used to provide connectivity with on-premises or other VPCs from a private subnet of the VPC. No elastic IP address is required to be assigned to a private NAT Gateway.

This cost can be minimized by placing a separate Public NAT Gateway in the Availability zones having instances with large volumes of Internet traffic. This Public NAT Gateway needs to be placed in the public subnet of the VPC and attach an Elastic IP address.

![disaster recovery comparison](DR.jpg)