+++
author = "L1bEn"
title = "AWS Server Side"
date = "2024-11-29"
description = "Especificar el lado del servidor con AWS KMS"
categories = [
    "AWS", "s3"
]
tags = [
    "AWS", "s3"
]
image = ""
+++

** Presentación breve**

Siempre descubrí que la mejor manera de entender algo es enseñarlo.

Hice un video para enseñar a usar AWS KMS. 

{{< youtube "6P6A6vQZMA8" >}}

{{< css.inline >}}
<style>
.canon { background: white; width: 100%; height: auto; }
</style>
{{< /css.inline >}}
