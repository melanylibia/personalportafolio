+++
author = "L1bSp"
title = "AWS S3 sends notifications"
date = "2024-12-01"
description = "Sends notifications from S3 to SNS to SQS to Lambda when an object is deleted"
categories = [
    "AWS", "S3", "SAM"
]
tags = [
    "AWS", "S3", "SAM"
]
image = ""
+++

**Brief Introduction**

I’ve always found that the best way to truly understand something is to teach it.

I made a video to teach how to Sends notifications from S3 to SNS to SQS to Lambda when an object is deleted using SAM tool.

{{< youtube "vU8L28DNuNs" >}}

{{< css.inline >}}
<style>
.canon { background: white; width: 100%; height: auto; }
</style>
{{< /css.inline >}}
