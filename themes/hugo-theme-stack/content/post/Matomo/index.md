+++
author = "L1bSp"
title = "Analytics in my blog??"
date = "2024-09-29"
description = "New features :)"
categories = [
    "Analytics",
]
tags = [
    "Analytics",
]
image = ""
+++

Hello, friends! Today, I’d like to share what I’ve learned about Matomo.

## **1. Integrating Matomo with in my System**

A few months ago, I designed my proxy server using Traefik. Integrating Matomo was a bit tricky, so I’ll start by showing you the completed configuration I used:

```
services:
  db:
    image: mariadb:10.11
    command: --max-allowed-packet=64MB
    volumes:
      - ./db:/var/lib/mysql:Z
    env_file:
      - ./db.env
    networks:
      - matomo_network
  app:
    image: matomo:fpm-alpine
    links:
      - db
    volumes:
      - ./matomo:/var/www/html:z
    env_file:
      - ./db.env
    networks:
      - matomo_network
  web:
    image: nginx:alpine
    restart: always
    volumes:
      - ./matomo:/var/www/html:z,ro
      - ./matomo.conf:/etc/nginx/conf.d/default.conf:z,ro
    networks:
      - traefik_apps
      - matomo_network
    labels:
      - traefik.docker.network=traefik_apps
      - traefik.enable=true
      - traefik.http.routers.matomo.rule=Host(`x.xlibia.xyz`)
      - traefik.http.services.matomo.loadbalancer.server.port=80
networks:
  traefik_apps:
    external: true
  matomo_network:
    driver: bridge
```

Next, I added this component `traefik`, but I encountered several issues because Matomo was retriving an IP from a different network. It's crucial to have 2 networks: one internal and one external. I had never used the traefik tag `traefik.docker.network=traefik_apps` before, but it proved to be very useful. I  initially thougth using the networks tag was enougth, but this tag assigns the Matomo server container to the global network, allowing it to receive an any IP from either network. I discovered this by using the following command:

```
docker network inspect traefik_apps
docker network inspect matomo_network
```

![matomo deployed](deployed.png)

## **2. Learning some basic from matomo**

I need to know some analitycs of my post, because i learn a little of matomo, I learn 2 things.

- Tracking Goals

**If you want to know how many people are visiting a specific page, such as an 'About' page, you need to set up a goal in Matomo.** 
This will help you monitor how many visitors are reaching About.

- Tracking Events

**I wanted to track how many people are being redirected to my SoundCloud podcast**. For this, you can use tracking events in Matomo.This way, you can track every time a visitor clicks on that link and gets redirected to your podcast.

{{< youtube "gOvpN-I9pqM" >}}

## off topic

If you are Bolivian, please take care of our forests.

{{< css.inline >}}
<style>
.canon { background: white; width: 100%; height: auto; }
</style>
{{< /css.inline >}}
