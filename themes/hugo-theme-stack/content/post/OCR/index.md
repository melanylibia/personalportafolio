+++
author = "L1bSp"
title = "OCR"
date = "2024-12-11"
description = "OCR and applications"
categories = [
    "Python", "OCR"
]
tags = [
    "Python", "OCR"
]
image = ""
+++

**Brief Introduction**

I’ve always found that the best way to truly understand something is to teach it.

{{< youtube "6PdUUOxwYQU" >}}

{{< css.inline >}}
<style>
.canon { background: white; width: 100%; height: auto; }
</style>
{{< /css.inline >}}
