+++
author = "L1bSp"
title = "What is Gitflow?"
date = "2025-01-29"
description = "Git Flow Process"
categories = [
    "Automation"
]
tags = [
    "Automation"
]
image = ""
+++

Acordly to [edrawmax](https://www.edrawmax.com/article/gitflow-diagram.html) Gitflow is simply organizing the work into branches and its sub-branches.

Git-flow is a small plug-in for Git. It is available on multiple operating systems. For OSX, we can install using CLI command brew install git-flow. For Window OS use this link to download.

_Example 1: create a release_

Let's assume that you work in a software development house, and your supervisor has assigned you a task, and now all your team members are working on the development branch. Directly from the develop branch, construct a feature branch for your assignment. Once you have finished your modifications, integrate your feature into the development branch. The main branch will be merged with the development branch by the deployment manager. The production servers use your main branch.

## git flow init

Initialize the flow, typically, you would use main for production and develop for development.

## Create a Feature Branch

Suppose if we want to work on a particular feature in the project, then we should create a feature branch.

```
git checkout develop && git pull origin develop && git flow feature start documentationv0.0.1

```

## End the feature

Once work is completed on the feature branch, then we should merge that feature branch into develop branch. Follow below steps to do that.
```
git add README.md 
git commit -m "Improve the documentation"
git flow feature finish documentationv0.0.1
```


If you observe the above summary of actions, it has automatically performed three actions.

## Release branch

Once develop branch has enough features for a release, then we need to create a release branch.

> Note: Release branches should be created from develop branch.

Once release branch is created that means new project cycle is started, so every new feature will be considered for the next release, not for current release, so we shouldn’t merge any feature directly to the release branch, we should only add bug fixes and release notes to release branch.

```
git flow release start v1.0
git add . 
git commit -m "Updated version of release"
git flow release finish v1.0
```

If you observe the above summary of actions, the release branch is merged with master and develop branches. the release branch is tagged with “0.1.0” and it is removed.

![Result](./result.png)

## Hotfix Branches

These hotfix branches are used for bug fixes and patches in production releases. With the fixes complete, time flies, and I'll soon need to write another post to elaborate.

## Advantages of Git-Flow:

in my case i think maintaining multiple branches in git is a little complicated, which can be maintained easily using Git-Flow.

## Resources
[The Gitflow Release Branch from Start to Finish](https://www.youtube.com/watch?v=rX80eKPdA28)