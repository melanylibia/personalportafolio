+++
author = "L1bSp"
title = "AWS Server Side"
date = "2024-11-28"
description = "Specifying server-side encryption with AWS KMS"
categories = [
    "AWS", "S3"
]
tags = [
    "AWS", "S3"
]
image = ""
+++

**Brief Introduction**

I’ve always found that the best way to truly understand something is to teach it.

I made a video to teach how to use AWS KMS.

{{< youtube "6P6A6vQZMA8" >}}

{{< css.inline >}}
<style>
.canon { background: white; width: 100%; height: auto; }
</style>
{{< /css.inline >}}
