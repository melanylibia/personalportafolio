+++
author = "L1bSp"
title = "Proxmox Basic"
date = "2025-02-19"
description = "Don't stop virtualizing, baby!"
categories = [
    "VM",
]
tags = [
    "Proxmox"
]
image = ""
+++


### Why Proxmox Lib?

I need to set up my K3s cluster, my friend! Before installing Proxmox, I had some experience, but now it's time to configure it on my own bare metal. I followed a helpful PDF guide that taught me more about the process.

[guide](https://www.virtualizationhowto.com/ebook/proxmox-home-lab-guide-2024.pdf)

### What is a Template?

We need to create a template! I followed a fantastic GitHub guide that provided awesome resources to get me started.

[ubuntu template](https://github.com/UntouchedWagons/Ubuntu-CloudInit-Docs)

### Running Everything Manually? Not Ideal!

Running things manually every time isn't the best approach. I prefer to use Infrastructure as Code (IaC) tools, and for that, I use Terraform to create my VMs. I followed a tutorial to guide me through this process. Remember, start with simple steps before diving into more complex configurations.

- [run a machine](https://austinsnerdythings.com/2021/08/30/how-to-create-a-proxmox-ubuntu-cloud-init-image/)
- [run a machine-incomplete simple to understand](https://spacelift.io/blog/terraform-proxmox-provider)

### Key Concepts You Need to Learn

Permissions are crucial in this setup. I was reading this amazing resource to understand them better, and it's made a huge difference.

- [permission doc](https://forum.proxmox.com/threads/permissions-and-roles.8418/)

## WE GOT IT, It small but it works

My first VM using terraform.

![test](test.png)

I found this [amazing tutorial](https://devopstales.github.io/cloud/proxmox-terraform).

## Let go for more

### And Lastly, My Friend...

I have to say, this is just the beginning! I plan to create more guides to help you through the journey.

![result](Screenshot_20250219_100706.png)

**Take care of your friend! 🐶**

{{< css.inline >}}
<style>
.canon { background: white; width: 100%; height: auto; }
</style>
{{< /css.inline >}}
