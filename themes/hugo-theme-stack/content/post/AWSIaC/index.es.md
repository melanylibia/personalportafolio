+++
author = "L1bEn"
title = "AWS cdk"
date = "2024-11-27"
description = "Usando el CDK AWS como herramienta de IaC"
categories = [
    "AWS", "IaC"
]
tags = [
    "AWS", "IaC"
]
image = ""
+++

** Presentación breve** 

Siempre descubrí que la mejor manera de entender algo es enseñarlo.

Hice un video para enseñar a usar el cdk de AWS para crear una función de Lambda. 

{{< youtube "GrYZ1soLuhU" >}}


{{< css.inline >}}
<style>
.canon { background: white; width: 100%; height: auto; }
</style>
{{< /css.inline >}}
