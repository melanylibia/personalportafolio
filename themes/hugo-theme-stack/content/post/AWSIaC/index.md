+++
author = "L1bSp"
title = "AWS cdk"
date = "2024-11-27"
description = "Using the AWS CDK as an IaC tool"
categories = [
    "AWS", "IaC"
]
tags = [
    "AWS", "IaC"
]
image = ""
+++

**Brief Introduction**

I’ve always found that the best way to truly understand something is to teach it.

I made a video to teach how to use AWS cdk to create a Lambda Function.

{{< youtube "GrYZ1soLuhU" >}}



{{< css.inline >}}
<style>
.canon { background: white; width: 100%; height: auto; }
</style>
{{< /css.inline >}}
