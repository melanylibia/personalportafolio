+++
author = "L1bSp"
title = "ALB with Lambda"
date = "2024-12-02"
description = "Application Load Balancer with Lambda as target"
categories = [
    "AWS", "Lambda", "Pulumi"
]
tags = [
    "AWS", "Lambda", "Pulumi"
]
image = ""
+++

**Brief Introduction**

I’ve always found that the best way to truly understand something is to teach it.

{{< youtube "wNhUffZOmD8" >}}

{{< css.inline >}}
<style>
.canon { background: white; width: 100%; height: auto; }
</style>
{{< /css.inline >}}
