 
+++
author = "L1bSp"
title = "What is WAF and  OWASP Juice Shop?"
date = "2025-02-02"
description = " OWASP Juice Shop  && WAF"
categories = [
    "Security"
]
tags = [
    "Security"
]
image = ""
+++

## OWASP Juice Shop

OWASP Juice Shop is a vulnerable web application, designed intentionally as insecure, and it can be used as a training tool. 

![maquina](original.png)

## Web Application Firewall (WAF)

As a first line of defense, we can use a Web Application Firewall (WAF). Basically a WAF protects web apps/APIs by inspecting HTTP requests. It acts at the layer 7 (Application Layer of the OSI Model) and it behaves like a reverse proxy with policies and rules attached to it to protect malicious requests, preventing them to flow to our servers or services. 

## Code 

```
services:
  bunkerweb:
    image: bunkerity/bunkerweb:1.5.0
    restart: always
    ports:
      - 80:8080
      - 443:8443
    labels:
      - "bunkerweb.INSTANCE"
    environment:
      - SERVER_NAME=web.marikita.online
      - USE_REVERSE_PROXY=yes
      - REVERSE_PROXY_URL=/
      - REVERSE_PROXY_HOST=http://myapp:3000
      - API_WHITELIST_IP=127.0.0.0/8 10.20.30.0/24
      - LIMIT_REQ_RATE=150r/s
    networks:
      - bw-universe
      - bw-services

  bw-scheduler:
    image: bunkerity/bunkerweb-scheduler:1.5.0
    restart: always
    depends_on:
      - bunkerweb
      - bw-docker
    volumes:
      - bw-data:/data
    environment:
      - DOCKER_HOST=tcp://bw-docker:2375
    networks:
      - bw-universe
      - bw-docker

  bw-docker:
    image: tecnativa/docker-socket-proxy
    restart: always
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock:ro
    environment:
      - CONTAINERS=1
    networks:
      - bw-docker
# Define container web service
  myapp:
    container_name: myapp
    restart: always
    image: bkimminich/juice-shop
    networks:
      - bw-services
    environment:
      - NODE_ENV=unsafe
volumes:
  bw-data:

networks:
  bw-universe:
    name: bw-universe
    ipam:
      driver: default
      config:
        - subnet: 10.20.30.0/24
  bw-services:
    name: bw-services
  bw-docker:
    name: bw-docker
```

## Test

_test 1 SQL injection in the login_

![msg](test0.png)
![msg1](test2.png)
![msg2](test3.png)


## Conclusion

I had to tried many times, the motto is add the dependency between the containers.

## Resources

[juice shop](https://github.com/juice-shop/juice-shop)
[original post](https://sys-ops.id/securing-docker-container-using-bunkerweb-single-site/)