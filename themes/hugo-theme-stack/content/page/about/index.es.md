---
title: creadora
description: Apasionada por FOSS y el blogging. 
date: '2019-02-28'
aliases:
  - contacto
license: CC BY-NC-ND
lastmod: '2020-10-09'
menu:
    main: 
        weight: -90
        params:
            icon: user
---

Este es mi sitio donde estoy en mis ratos libres. Algunas actividades que realizo.

*  [souncloud](https://soundcloud.com/the_tech_shortcut)

*  [mi otro blog](libialany.github.io)

*  [project videos](https://www.youtube.com/@libops-m8l)

*  [old videos](https://www.youtube.com/@projects3924/videos)

*  [linkedin](https://www.linkedin.com/in/melany-e)