---
title: About
description: The work i've done
date: '2019-02-28'
aliases:
  - contact
license: CC BY-NC-ND
lastmod: '2020-10-09'
menu:
    main: 
        weight: -90
        params:
            icon: user
---

This is my _LINK TREE_

* <a class="link" href="https://drive.google.com/file/d/1Sve7-QRgDSQ1CMEu1xK9unIL4uG44RXG/view?usp=sharing" target="_blank" rel="noopener">MY CV  👈</a>

* <a href="https://soundcloud.com/the_tech_shortcut" title="souncloud" onclick="_paq.push(['trackEvent', 'Souncloud', 'Soundcloud Link Click', 'soundcloud_visitor']);">souncloud</a>

*  [other blog](libialany.github.io)

*  [project videos](https://www.youtube.com/@libops-m8l)

*  [old videos](https://www.youtube.com/@projects3924/videos)


